class CustomArray < Array
  
  def initialize(length)
    super(length, 0)
  end

  def squish(arr, n = nil)
    arr.each_with_object([]) do |element, flattened|
      flattened.push *(element.is_a?(Array) && n != nil && element.size <= n ? squish(element, n) : element)
    end
  end

end

a = Foo.new(5)
p a
#p squish([1])         # [1]
#p squish([[1],2])     # [1, 2]
#p squish([[1,[2]]],1) # [1, [2]]

"""
require 'minitest/autorun'

class Tests < MiniTest::Unit::TestCase
  def test_example_input
    assert_equal 3, flags([1, 5, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2])
  end

  def test_one_peak
    assert_equal 1, flags([1, 1, 2, 1, 1, 1])
  end

  def test_three_flags
    assert_equal 3, flags([1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1])
  end

  def test_no_peak
    assert_equal 0, flags([1, 1, 4, 5, 6])
  end
end
"""
