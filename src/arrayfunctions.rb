#everything is better wrapped in within a class
class ArrayFunctions

  def self.squish(arr, n = nil)
    begin
      arr.each_with_object([]) do |element, flattened|
        flattened.push *(element.is_a?(Array) && n != nil && element.size <= n ? squish(element, n) : element)
      end
    rescue
      puts "Unexpected error: code ###"
      #log or treat exception different
    end
  end

end