require './src/arrayfunctions'

p ArrayFunctions.squish([[1,2,[3]],4],1) # [1, 2, [3], 4]
p ArrayFunctions.squish([1])         # [1]
p ArrayFunctions.squish([[1],2])     # [1, 2]
p ArrayFunctions.squish([[1,[2]]],1) # [1, [2]]
