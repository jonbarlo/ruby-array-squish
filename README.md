# Array flatten not using ruby's flatten

###folder structure
|-- .this_folder_path
    |-- Gemfile
    |-- Gemfile.lock
    |-- Guardfile
    |-- Rakefile
    |-- README.md
    |-- sample
        |-- hellosquish.rb
    |-- src
        |-- arrayfunctions.rb
    |-- test
        |-- unit
            |-- arrayfunctions_test.rb
###run app
    require './arrayfunctions'
    #not required to create an instance
    #ArrayFunctions
    puts ArrayFunctions.squish([1])

###run test
    ruby arrayfunctions_test.rb


2016 Jonathan Barquero