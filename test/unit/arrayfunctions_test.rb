require 'minitest/autorun'
require './src/arrayfunctions'

class Tests < MiniTest::Unit::TestCase

  #test with default example data provided on http://careers.citrusbyte.com/apply/tPTZOv/Experienced-Software-Developer
  def test_with_website_data
    array = [[1,2,[3]],4]
    #it should be 2
    assert_equal array.size, 2
    array = ArrayFunctions.squish(array, 1)
    #it should be increased by one
    assert_equal array.size, 4
  end

  #one level of nesting flatten
  def test_nested_2_level
    array = [1,[2, 3]]
    #it should be 2
    assert_equal array.size, 2
    array = ArrayFunctions.squish(array, 1)
    #it should be increased by one
    assert_equal array.size, 3
  end

  #two levels of nesting flatten
  def test_nested_3_levels
    array = [1,[2, 3, [4,5,6]]]
    #it should be 2
    assert_equal array.size, 2
    array = ArrayFunctions.squish(array, 3)
    #it should be increased by one
    assert_equal array.size, 6
  end
  
end
